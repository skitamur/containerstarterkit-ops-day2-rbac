# Starter Kit : 運用編 RBAC ハンズオン

Container Stater Kit で実施するハンズオンの手順を紹介します。  
当ハンズオンによって、OpenShift内の権限管理を体験することができます。

具体的な手順についてはhandsonフォルダのREADME.mdを参照ください。
---
## [RBACハンズオン手順](./handson/README.md)